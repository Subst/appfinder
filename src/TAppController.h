#ifndef TAPPCONTROLLER_H
#define TAPPCONTROLLER_H

#include <QObject>
#include "windows.h"

// структура для хранения информации об окне, избыточна для нынешних целей, но может завтра понадобится
struct WindowInfo
 {
 WindowInfo(HWND handle=NULL,QString windowTitle=QString())
  {
  this->handle=handle;
  this->windowTitle=windowTitle;
  }

 HWND handle;         // handle окна
 QString windowTitle; // титл конкретного окна
 };

class TAppController : public QObject
 {
 Q_OBJECT
 public:
  enum Direction
   {
   Up,
   Right,
   Down,
   Left
   };
  Q_ENUM(Direction)

  explicit TAppController(QObject *parent = nullptr);
  QList <WindowInfo> getWindowList();
  bool sendMessage(HWND,Direction);

 protected:
  static BOOL CALLBACK EnumWindowsProc(HWND,LPARAM); // callback функции должны быть "свободными", поскольку у нас завернуто все в класс, то "свободная" - static
  HWND getEffectiveHwnd(HWND,QString,QString);

 private:
  static QList <WindowInfo> m_windowInfo; // работаем со списком этим в static функции, потому и сам он static

 signals:

 };

#endif // TAPPCONTROLLER_H
