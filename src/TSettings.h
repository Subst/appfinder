#ifndef TSETTINGS_H
#define TSETTINGS_H

#include <QByteArray>
#include <QString>
#include <QVariant>
#include <QApplication>

class TSettings
 {
 //Q_OBJECT
 public:
  TSettings(QObject *parent);

  // ini файл, работа с кокретным значением
  static void setIniValue(QString group, QString key, QVariant value,
                         QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".ini");
  static QVariant getIniValue(QString group,QString key,QVariant defaultValue,
                             QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".ini");

  //static QStringList getKeys(QString group,QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".ini");

  // xml файл, значение атрибута key в path или текста path
  static void setXmlValue(QStringList path, QString key, QVariant value,
                          QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");
  static void setXmlValue(QString path, QString key, QVariant value,
                          QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");

  static QVariant getXmlValue(QStringList path, QString key, QVariant defaultValue=QVariant(),
                              QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");
  static QVariant getXmlValue(QString path, QString key, QVariant defaultValue=QVariant(),
                              QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");

  // xml файл, список наборов всех значений, для всех найденных path
  static void setXmlMaps(QStringList path,bool isAttrs,QList<QMap<QString,QVariant>> values,
                         QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");
  static void setXmlMaps(QString path,bool isAttrs,QList<QMap<QString, QVariant>> values,
                         QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");

  static QList<QMap<QString, QVariant>> getXmlMaps(QStringList path,bool isAttrs,
                                                   QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");
  static QList<QMap<QString, QVariant>> getXmlMaps(QString path, bool isAttrs,
                                                   QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");

  // может далее возращать новый список а не void?
  static void appendXmlMap(QStringList path, bool isAttrs, QMap <QString,QVariant> value,
                           QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");
  static void appendXmlMap(QString path, bool isAttrs, QMap <QString,QVariant> value,
                           QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");

  static void appendXmlMaps(QStringList path, bool isAttrs, QList <QMap <QString,QVariant>> values,
                           QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");
  static void appendXmlMaps(QString path, bool isAttrs, QList <QMap <QString,QVariant>> values,
                           QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");

  static void removeXmlMap(QStringList path, bool isAttrs, QMap <QString,QVariant> value,
                           QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");
  static void removeXmlMap(QString path, bool isAttrs, QMap <QString,QVariant> value,
                           QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");

  static void removeXmlMaps(QStringList path, bool isAttrs, QList <QMap <QString,QVariant>> values,
                           QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");
  static void removeXmlMaps(QString path, bool isAttrs, QList <QMap <QString,QVariant>> values,
                           QString fileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");

  // это для моих целей, перетащить ini в xml, дабы настройки при обновлении программы сохранить
  static void iniToXml(bool remove=false,QString fromFileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".ini",
                       QString toFileName=qApp->applicationDirPath()+"/"+qApp->applicationName().toLower()+".xml");

 protected:
  // то для удобства представления, видеть смысл записей просто открыв XML
  static QString variantToString(const QVariant &variant);
  static QVariant stringToVariant(const QString &string);
 private:

 };

#endif // TSETTINGS_H

