#include "TAppController.h"

#include <QDebug>

#include "psapi.h"
#include "winver.h"
#include "strsafe.h"

QList <WindowInfo> TAppController::m_windowInfo;

TAppController::TAppController(QObject *parent) : QObject(parent)
 {

 }
//
QList<WindowInfo> TAppController::getWindowList()
 {
 m_windowInfo.clear();
 EnumWindows(&EnumWindowsProc,0);
 return m_windowInfo;
 }
//
bool TAppController::sendMessage(HWND hWnd, Direction direction)
 {
 bool result=true; // возвращать будем
 WPARAM wParam=0;
 switch (direction)
  {
  case Up:
   {
   wParam=VK_UP;
   break;
   }
  case Right:
   {
   wParam=VK_RIGHT;
   break;
   }
  case Down:
   {
   wParam=VK_DOWN;
   break;
   }
  case Left:
   {
   wParam=VK_LEFT;
   break;
   }
  }

 wchar_t wTitle[MAX_PATH];
 GetWindowText(hWnd,wTitle,sizeof(wTitle));

 wchar_t wClassName[MAX_PATH];
 GetClassName(hWnd,wClassName,sizeof(wClassName));

 HWND foregroundHwnd=GetForegroundWindow(); // запоминаю окно переднего плана
 HWND handle=getEffectiveHwnd(hWnd,QString::fromWCharArray(wTitle),QString::fromWCharArray(wClassName));
 if (!handle)
  return false;

 SetForegroundWindow(handle);//hWnd); // устанавливаю целевое на передний план, почему то только так срабатывает

 INPUT input;
 input.type=INPUT_KEYBOARD;
 input.ki.wScan=0; // hardware scan code for key
 input.ki.time=0;
 input.ki.dwExtraInfo=0;

 input.ki.wVk=wParam;
 input.ki.dwFlags=0; // Нажать
 result &= SendInput(1,&input,sizeof(INPUT));

 input.ki.dwFlags=KEYEVENTF_KEYUP; // Отпустить
 result &= SendInput(1,&input,sizeof(INPUT));

 Sleep(100); // пауза нужна, чтобы успел отработать рендеринг целевого окна
 SetForegroundWindow(foregroundHwnd); // возвращаю окно переднего плана на место
 return result;
 }
//
BOOL CALLBACK TAppController::EnumWindowsProc(HWND hWnd,LPARAM lParam)
 {
 wchar_t wTitle[MAX_PATH];
 if (IsWindowVisible(hWnd) && GetWindowText(hWnd,wTitle,sizeof(wTitle))) // если окно видимое, получаем имя окна
  {
  WindowInfo info(hWnd,QString::fromWCharArray(wTitle));
  m_windowInfo.append(info);
  }
 return TRUE;
 }
//
HWND TAppController::getEffectiveHwnd(HWND hWnd,QString title,QString className)
 {
 if (className.contains("Chrome",Qt::CaseInsensitive)) // если в названии окна есть Chrome, это Хром или что-то еще на базе Хромиум
  return hWnd;

 HWND handle=NULL;
 handle=FindWindowEx(hWnd,NULL,L"paneClassDC",NULL); // из чтения форумов следует, что реально paneClassDC - дочерний класс презентации PowerPoint
 if (handle)
  return handle;

 handle=FindWindowEx(hWnd,NULL,L"Windows.UI.Core.CoreWindow",NULL); // Windows.UI.Core.CoreWindow - дочерний класс презентации PowerPoint Mobile

 return handle;
 }
//
