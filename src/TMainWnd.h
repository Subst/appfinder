﻿#ifndef TMAINWND_H
#define TMAINWND_H

#include "ui_TMainWnd.h"
#include <QSignalMapper>

class TMainWnd : public QMainWindow, private Ui::TMainWnd
 {
 Q_OBJECT
 public:
  TMainWnd(QWidget *parent = 0);

 protected:
  void initCore();
  void initIface();

  void writeSettings();
  void readSettings();

  void closeEvent(QCloseEvent *event);

 private:
  QSignalMapper *m_keyMapper;

 protected slots:
  void aboutToQuit();
  void about();

  void getList();
  void keyMapperMapped(int);

 };

#endif // TMAINWND_H
