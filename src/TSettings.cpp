#include "TSettings.h"
#include <QDebug>
#include <QSettings>
#include <QFile>
#include <QFileInfo>
#include <QTextCodec>

#include <QDomDocument>
#include <QDomNode>

#include <QRect>
#include <QDataStream>

#include <QDateTime>
#include <QColor>
#include <QFont>

TSettings::TSettings(QObject *parent)
 {

 }
//
void TSettings::setIniValue(QString group, QString key, QVariant value, QString fileName)
 {
 QSettings settings(fileName,QSettings::IniFormat);
 settings.setIniCodec(QTextCodec::codecForName("UTF-8"));

 settings.beginGroup(group);
 settings.setValue(key,value);
 settings.endGroup();
 return;
 }
//
QVariant TSettings::getIniValue(QString group, QString key, QVariant defaultValue, QString fileName)
 {
 QSettings settings(fileName,QSettings::IniFormat);
 settings.setIniCodec(QTextCodec::codecForName("UTF-8"));

 settings.beginGroup(group);
 QVariant value=settings.value(key,defaultValue);
 settings.endGroup();

 return value;
 }
//
/*QStringList TCommon::getKeys(QString group ,QString fileName)
 {
 QStringList keys;
 QSettings settings(fileName,QSettings::IniFormat);
 settings.setIniCodec(QTextCodec::codecForName("UTF-8"));
 settings.beginGroup(group);
  keys=settings.childKeys();
 settings.endGroup();
 return keys;
 }*/
//
void TSettings::setXmlValue(QStringList path, QString key, QVariant value, QString fileName)
 {
 QDomDocument document("");
 if (QFileInfo(fileName).path().isEmpty())
  fileName.prepend(qApp->applicationDirPath()+"/");
 QString info=QFileInfo(fileName).baseName();

 QFile file(fileName);
 QByteArray data;
 if (file.open(QIODevice::ReadOnly))
  {
	data=file.readAll();
	file.close();
	}
 if (!document.setContent(data))
	document=QDomDocument();

 QDomNode node=document.firstChildElement();
 if (node.isNull() || node.toElement().nodeName()!=info)
  {
  document.clear();
  document.appendChild(document.createProcessingInstruction("xml","version='1.0' encoding='UTF-8'"));
  document.appendChild(document.createComment(QString("The %1 settings").arg(info)));
  node=document.createElement(info);
  document.appendChild(node);
  }

 QDomElement element;
 foreach (QString section,path)
  {
  element=node.firstChildElement(section);
  if (element.isNull())
   {
   element=document.createElement(section);
   node=node.appendChild(element);
   }
  else
   node=element;
  }

 QString string=variantToString(value);

 // если key не задан, то value пишется как текст в node, иначе как аттрибут
 // может стоит явно указывать куда писать? типа enum TCommon::valueType (NodeType,AttributeType)
 if (key.isEmpty())
  {
  QDomText text=document.createTextNode(string);
  if (node.firstChild().isNull())
   node.appendChild(text);
  else
   node.replaceChild(text,node.firstChild());
  }
 else
  node.toElement().setAttribute(key,string);

 if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate))
  return;

 QTextStream stream(&file);
 stream.setCodec("UTF-8");
 stream << document.toString(2);
 file.close();
 return;
 }
//
void TSettings::setXmlValue(QString path, QString key, QVariant value, QString fileName)
 {
 setXmlValue(path.split("/",QString::SkipEmptyParts),key,value,fileName);
 return;
 }
//
QVariant TSettings::getXmlValue(QStringList path, QString key, QVariant defaultValue, QString fileName)
 {
 QString string;
 QDomDocument document("");
 if (QFileInfo(fileName).path().isEmpty())
  fileName.prepend(qApp->applicationDirPath()+"/");
 QString info=QFileInfo(fileName).baseName();

 QFile file(fileName);
 QByteArray data;
 if (file.open(QIODevice::ReadOnly))
  {
	data=file.readAll();
	file.close();
	}
 if (!document.setContent(data))
	return QVariant();

 QDomNode node=document.firstChildElement();
 if (node.isNull() || node.toElement().nodeName()!=info)
  return defaultValue;

 QDomElement element;
 foreach (QString section,path)
  {
  element=node.firstChildElement(section);
  if (element.isNull())
   return defaultValue;
  else
   node=element;
  }

 // если key не задан, то value возращается как текст в node, иначе как аттрибут node
 // может стоит явно указывать куда писать? типа enum TCommon::valueType (NodeType,AttributeType)
 if (key.isEmpty())
  {
  if (node.firstChild().isNull())
   return defaultValue;
  else
   string=node.firstChild().toText().data();
  }
 else
  string=node.toElement().attribute(key,defaultValue.toString());

 QVariant value=stringToVariant(string);
 return value;
 }
//
QVariant TSettings::getXmlValue(QString path, QString key, QVariant defaultValue, QString fileName)
 {
 return getXmlValue(path.split("/",QString::SkipEmptyParts),key,defaultValue,fileName);
 }
//
//
//
void TSettings::setXmlMaps(QStringList path, bool isAttrs, QList<QMap<QString,QVariant>> values, QString fileName)
 {
 QDomDocument document("");
 if (QFileInfo(fileName).path().isEmpty())
  fileName.prepend(qApp->applicationDirPath()+"/");
 QString info=QFileInfo(fileName).baseName();

 QFile file(fileName);
 QByteArray data;
 if (file.open(QIODevice::ReadOnly))
  {
	data=file.readAll();
	file.close();
	}
 if (!document.setContent(data))
	document=QDomDocument();

 QDomNode node=document.firstChildElement();
 if (node.isNull() || node.toElement().nodeName()!=info)
  {
  document.clear();
  document.appendChild(document.createProcessingInstruction("xml","version='1.0' encoding='UTF-8'"));
  document.appendChild(document.createComment(QString("The %1 settings").arg(info)));
  node=document.createElement(info);
  document.appendChild(node);
  }

 QDomElement element;
 QString section;

 for (int i=0;i<path.size()-1;i++)
  {
  section=path.at(i);
  element=node.firstChildElement(section);
  if (element.isNull())
   {
   element=document.createElement(section);
   node=node.appendChild(element);
   }
  else
   node=element;
  }
 // грохнуть все child этого узла
 section=path.last();
 while (!node.firstChildElement(section).isNull())
  node.removeChild(node.firstChildElement(section));

 // теперь создать заново, исходя из данных values
 for (int i=0;i<values.size();i++)
  {
  QMap <QString,QVariant> attrs=values.at(i);
  element=document.createElement(section);
  QDomNode child=node.appendChild(element);

  foreach (QString key,attrs.keys())
   {
   QString string=variantToString(attrs.value(key));
   if (isAttrs) // если атрибуты
    child.toElement().setAttribute(key,string);
   else // дочерние узлы с текстом
    {
    QDomElement element=document.createElement(key);
    QDomText text=document.createTextNode(string);
    element.appendChild(text);
    child.appendChild(element);
    }
   }
  }

 if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate))
  return;

 QTextStream stream(&file);
 stream.setCodec("UTF-8");
 stream << document.toString(2);
 file.close();
 return;
 }
//
void TSettings::setXmlMaps(QString path, bool isAttrs, QList<QMap<QString, QVariant>> values, QString fileName)
 {
 setXmlMaps(path.split("/",QString::SkipEmptyParts),isAttrs,values,fileName);
 return;
 }
//
QList <QMap<QString,QVariant>> TSettings::getXmlMaps(QStringList path, bool isAttrs, QString fileName)
 {
 QString string=QString();
 QDomDocument document("");
 if (QFileInfo(fileName).path().isEmpty())
  fileName.prepend(qApp->applicationDirPath()+"/");
 QString info=QFileInfo(fileName).baseName();

 QList <QMap <QString,QVariant>> results=QList<QMap <QString,QVariant>>(); // будет возвращаться

  QFile file(fileName);
 QByteArray data;
 if (file.open(QIODevice::ReadOnly))
  {
	data=file.readAll();
	file.close();
	}
 if (!document.setContent(data))
	return result;

 QDomNode node=document.firstChildElement();
 if (node.isNull() || node.toElement().nodeName()!=info)
  return results;

 QDomElement element;
 QString section=QString();
 foreach (section,path)
  {
  element=node.firstChildElement(section);
  if (element.isNull())
   return results;
  else
   node=element;
  }

 while (!node.isNull())
  {
  QMap <QString,QVariant> values=QMap <QString,QVariant>(); // для текущих значений одного узла
  if (isAttrs) // если атрибуты
   {
   QDomNamedNodeMap attributes=node.attributes();
   for (int i=0;i<attributes.size();i++)
    {
    QDomAttr attribute=attributes.item(i).toAttr();
    values.insert(attribute.name(),stringToVariant(attribute.value()));
    }
   }
  else
   {
   QDomNodeList nodes=node.childNodes();// тут читаем дочерние узлы и их тексты
   for (int i=0;i<nodes.size();i++)
    {
    QDomNode child=nodes.item(i);
    values.insert(child.nodeName(),stringToVariant(child.firstChild().toText().data()));
    }
   }
  results<<values;
  node=node.nextSiblingElement(section);
  }
 return results;
 }
//
QList <QMap <QString,QVariant>> TSettings::getXmlMaps(QString path, bool isAttrs, QString fileName)
 {
 return getXmlMaps(path.split("/",QString::SkipEmptyParts),isAttrs,fileName);
 }
//
void TSettings::appendXmlMap(QStringList path, bool isAttrs, QMap <QString,QVariant> value, QString fileName)
 {
 QList <QMap <QString,QVariant>> results=getXmlMaps(path,isAttrs,fileName);
 if (!results.contains(value))
  {
  results.append(value);
  setXmlMaps(path,isAttrs,results,fileName);
  }
 return;
 }
//
void TSettings::appendXmlMap(QString path,bool isAttrs, QMap <QString,QVariant> value, QString fileName)
 {
 appendXmlMap(path.split("/",QString::SkipEmptyParts),isAttrs,value,fileName);
 return;
 }
//
void TSettings::appendXmlMaps(QStringList path,bool isAttrs,QList <QMap <QString,QVariant>> values, QString fileName)
 {
 QList <QMap <QString,QVariant>> results=getXmlMaps(path,isAttrs,fileName);
 for (int i=0;i<values.size();i++)
  {
  QMap <QString,QVariant> value=values.at(i);
  if (!results.contains(value))
   results.append(value);
  }
 setXmlMaps(path,isAttrs,results,fileName);
 return;
 }
//
void TSettings::appendXmlMaps(QString path,bool isAttrs, QList <QMap <QString,QVariant>> values, QString fileName)
 {
 appendXmlMaps(path.split("/",QString::SkipEmptyParts),isAttrs,values,fileName);
 return;
 }
//
void TSettings::removeXmlMap(QStringList path,bool isAttrs,QMap <QString,QVariant> value, QString fileName)
 {
 QList <QMap <QString,QVariant>> results=getXmlMaps(path,isAttrs,fileName);
 results.removeAll(value);
 setXmlMaps(path,isAttrs,results,fileName);
 return;
 }
//
void TSettings::removeXmlMap(QString path,bool isAttrs, QMap <QString,QVariant> value, QString fileName)
 {
 removeXmlMap(path.split("/",QString::SkipEmptyParts),isAttrs,value,fileName);
 return;
 }
//
void TSettings::removeXmlMaps(QStringList path,bool isAttrs,QList <QMap <QString,QVariant>> values, QString fileName)
 {
 QList <QMap <QString,QVariant>> results=getXmlMaps(path,isAttrs,fileName);
 for (int i=0;i<values.size();i++)
  {
  QMap <QString,QVariant> value=values.at(i);
  results.removeAll(value);
  }
 setXmlMaps(path,isAttrs,results,fileName);
 return;
 }
//
void TSettings::removeXmlMaps(QString path,bool isAttrs, QList <QMap <QString,QVariant>> values, QString fileName)
 {
 removeXmlMaps(path.split("/",QString::SkipEmptyParts),isAttrs,values,fileName);
 return;
 }
//
//
//
QString TSettings::variantToString(const QVariant &variant)
 {
 QString string;
 switch (variant.type())
  {
  case QVariant::Invalid:
   {
   string=QLatin1String("@Invalid()");
   break;
   }
  case QVariant::ByteArray:
   {
   QByteArray bytes=variant.toByteArray().toHex().toUpper().prepend("0x");
   string=QLatin1String("@ByteArray(")+QLatin1String(bytes.constData(),bytes.size())+QLatin1Char(')');
   break;
   }
  case QVariant::String:
  case QVariant::LongLong:
  case QVariant::ULongLong:
  case QVariant::Int:
  case QVariant::UInt:
  case QVariant::Bool:
  case QVariant::Double:
  case QVariant::KeySequence:
   {
   string=variant.toString();
   if (string.contains(QChar::Null))
    string=QLatin1String("@String(")+string+QLatin1Char(')');

   if (string.startsWith(QLatin1Char('@')))
    string.prepend(QLatin1Char('@'));
   break;
   }
  case QVariant::Rect:
   {
   QRect rect=qvariant_cast<QRect>(variant);
   string=QString("@Rect(%1,%2,%3,%4)").arg(rect.x()).arg(rect.y()).arg(rect.width()).arg(rect.height());
   break;
   }
  case QVariant::RectF:
   {
   QRectF rect=qvariant_cast<QRectF>(variant);
   string=QString("@RectF(%1,%2,%3,%4)").arg(rect.x(),0,'f',3).arg(rect.y(),0,'f',3).arg(rect.width(),0,'f',3).arg(rect.height(),0,'f',3);
   break;
   }
  case QVariant::Size:
   {
   QSize size=qvariant_cast<QSize>(variant);
   string=QString("@Size(%1,%2)").arg(size.width()).arg(size.height());
   break;
   }
  case QVariant::SizeF:
   {
   QSizeF size=qvariant_cast<QSizeF>(variant);
   string=QString("@SizeF(%1,%2)").arg(size.width(),0,'f',3).arg(size.height(),0,'f',3);
   break;
   }
  case QVariant::Point:
   {
   QPoint point=qvariant_cast<QPoint>(variant);
   string=QString("@Point(%1,%2)").arg(point.x()).arg(point.y());
   break;
   }
  case QVariant::PointF:
   {
   QPointF point=qvariant_cast<QPointF>(variant);
   string=QString("@PointF(%1,%2)").arg(point.x(),0,'f',3).arg(point.y(),0,'f',3);
   break;
   }
  case QVariant::Date:
   {
   QDate date=qvariant_cast<QDate>(variant);
   string=QString("@Date(%1,%2,%3)").arg(date.year()).arg(date.month()).arg(date.day());
   break;
   }
  case QVariant::Time:
   {
   QTime time=qvariant_cast<QTime>(variant);
   string=QString("@Time(%1,%2,%3,%4)").arg(time.hour()).arg(time.minute()).arg(time.second()).arg(time.msec());
   break;
   }
  case QVariant::DateTime:
   {
   QDateTime dateTime=qvariant_cast<QDateTime>(variant);
   string=QString("@DateTime(%1,%2,%3,%4,%5,%6,%7,%8)").arg(dateTime.date().year()).arg(dateTime.date().month()).arg(dateTime.date().day()).
                    arg(dateTime.time().hour()).arg(dateTime.time().minute()).arg(dateTime.time().second()).arg(dateTime.time().msec()).arg(dateTime.timeSpec());
   break;
   }
  case QVariant::Color:
   {
   QColor color=qvariant_cast<QColor>(variant);
   string=QString("@Color(0x%1)").arg(QString::number(color.rgba(),16).toUpper());
   break;
   }
  case QVariant::Font:
   {
   QFont font=qvariant_cast<QFont>(variant);
   string="@Font("+font.toString()+")";
   //QTextStream(&string)<<"@Font("<<font.toString()<<")";
   break;
   }
  case QVariant::StringList:
   {
   string="@StringList("+variant.toStringList().join(";")+")";
   break;
   }
  default:
   {
#ifndef QT_NO_DATASTREAM
   QByteArray bytes;
   QDataStream stream(&bytes,QIODevice::WriteOnly);
   stream<<variant;
   bytes=bytes.toHex().toUpper().prepend("0x");
   string=QLatin1String("@Variant(")+QLatin1String(bytes.constData(),bytes.size())+QLatin1Char(')');
#else
            Q_ASSERT(!"QSettings: Cannot save custom types without QDataStream support");
#endif
   break;
   }
  }
 return string;
 }
//
QVariant TSettings::stringToVariant(const QString &string)
 {
 QStringList args;
 if (string.startsWith(QLatin1Char('@')) && string.endsWith(QLatin1Char(')')))
  {
  if (string.startsWith(QLatin1String("@ByteArray(0x")))
   return QVariant(QByteArray::fromHex(string.mid(13,string.size()-14).toLatin1()));

  if (string.startsWith(QLatin1String("@String(")))
   return QVariant(string.mid(8,string.size()-9));

  if (string.startsWith(QLatin1String("@Rect(")))
   {
   args=string.mid(6,string.size()-7).split(",");
   if (args.size()==4)
    return QVariant(QRect(args[0].toInt(),args[1].toInt(),args[2].toInt(),args[3].toInt()));
   }

  if (string.startsWith(QLatin1String("@RectF(")))
   {
   args=string.mid(7,string.size()-8).split(",");
   if (args.size()==4)
    return QVariant(QRectF(args[0].toFloat(),args[1].toFloat(),args[2].toFloat(),args[3].toFloat()));
   }

  if (string.startsWith(QLatin1String("@Size(")))
   {
   args=string.mid(6,string.size()-7).split(",");
   if (args.size()==2)
    return QVariant(QSize(args[0].toInt(),args[1].toInt()));
   }

  if (string.startsWith(QLatin1String("@SizeF(")))
   {
   args=string.mid(7,string.size()-8).split(",");
   if (args.size()==2)
    return QVariant(QSizeF(args[0].toFloat(),args[1].toFloat()));
   }

  if (string.startsWith(QLatin1String("@Point(")))
   {
   args=string.mid(7,string.size()-8).split(",");
   if (args.size()==2)
    return QVariant(QPoint(args[0].toInt(),args[1].toInt()));
   }

  if (string.startsWith(QLatin1String("@PointF(")))
   {
   args=string.mid(8,string.size()-9).split(",");
   if (args.size()==2)
    return QVariant(QPointF(args[0].toFloat(),args[1].toFloat()));
   }

  if (string.startsWith(QLatin1String("@Date(")))
   {
   args=string.mid(6,string.size()-7).split(",");
   if (args.size()==3)
    return QVariant(QDate(args[0].toInt(),args[1].toInt(),args[2].toInt()));
   }

  if (string.startsWith(QLatin1String("@Time(")))
   {
   args=string.mid(6,string.size()-7).split(",");
   if (args.size()==4)
    return QVariant(QTime(args[0].toInt(),args[1].toInt(),args[2].toInt(),args[3].toInt()));
   }

  if (string.startsWith(QLatin1String("@DateTime(")))
   {
   args=string.mid(10,string.size()-11).split(",");
   if (args.size()==8)
    return QVariant(QDateTime(QDate(args[0].toInt(),args[1].toInt(),args[2].toInt()),
                              QTime(args[3].toInt(),args[4].toInt(),args[5].toInt(),args[6].toInt()),Qt::TimeSpec(args[7].toInt())));
   }

  if (string.startsWith(QLatin1String("@Color(")))
   return QVariant(QColor(string.mid(7,string.size()-8).remove("0x").toUInt(nullptr,16)));

  if (string.startsWith(QLatin1String("@Font(")))
   {
   QFont font;
   if (font.fromString(string.mid(6,string.size()-7)))
    return QVariant(font);
   }

  if (string.startsWith(QLatin1String("@StringList(")))
   return QVariant(string.mid(12,string.size()-13).split(";"));

  if (string.startsWith(QLatin1String("@Variant(0x")))
   {
#ifndef QT_NO_DATASTREAM
   QByteArray bytes=QByteArray::fromHex(string.mid(11,string.size()-12).toLatin1());
   QDataStream stream(&bytes,QIODevice::ReadOnly);
   QVariant variant;
   stream >> variant;
   return variant;
#else
                Q_ASSERT(!"QSettings: Cannot load custom types without QDataStream support");
#endif
   }
  if (string==QLatin1String("@Invalid()"))
   return QVariant();

  if (string.startsWith(QLatin1String("@@")))
   return QVariant(string.mid(1));
  }
 return QVariant(string);
 }
//
void TSettings::iniToXml(bool remove,QString fromFileName, QString toFileName)
 {
 if (QFileInfo(fromFileName).path().isEmpty())
  fromFileName.prepend(qApp->applicationDirPath()+"/");
 if (!QFile::exists(fromFileName))
  return;

 if (QFileInfo(toFileName).path().isEmpty())
  toFileName.prepend(qApp->applicationDirPath()+"/");

 QSettings settings(fromFileName,QSettings::IniFormat);
 settings.setIniCodec(QTextCodec::codecForName("UTF-8"));

 foreach(QString key,settings.allKeys())
  setXmlValue(key,"",getIniValue("",key,""));

 if (remove)
  {
  QFile file(fromFileName);
  file.remove();
  }
 return;
 }
