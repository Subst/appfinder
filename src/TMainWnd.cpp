﻿#include "TMainWnd.h"

#include <QMessageBox>
#include <QCloseEvent>
#include <QDebug>
#include "TAppController.h"

TMainWnd::TMainWnd(QWidget *parent) : QMainWindow(parent)
 {
 setupUi(this);
 initCore();
 //initIface();
 }
//
void TMainWnd::initCore()
 {
 connect(getListBtn,&QPushButton::clicked,this,&TMainWnd::getList);

 m_keyMapper=new QSignalMapper(this);
 connect(m_keyMapper,QOverload<int>::of(&QSignalMapper::mapped),this,&TMainWnd::keyMapperMapped);

 connect(upBtn,&QPushButton::clicked,m_keyMapper,static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
 m_keyMapper->setMapping(upBtn,(int)TAppController::Up);

 connect(rightBtn,&QPushButton::clicked,m_keyMapper,static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
 m_keyMapper->setMapping(rightBtn,(int)TAppController::Right);

 connect(downBtn,&QPushButton::clicked,m_keyMapper,static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
 m_keyMapper->setMapping(downBtn,(int)TAppController::Down);

 connect(leftBtn,&QPushButton::clicked,m_keyMapper,static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
 m_keyMapper->setMapping(leftBtn,(int)TAppController::Left);
 return;
 }
//
void TMainWnd::initIface()
 {

 return;
 }
//
void TMainWnd::writeSettings()
 {

 return;
 }
//
void TMainWnd::readSettings()
 {

 return;
 }
//
void TMainWnd::closeEvent(QCloseEvent *event)
 {
 if (QMessageBox::question(this,tr("Confirmation"),tr("Exit program?"),QMessageBox::Yes | QMessageBox::No)==QMessageBox::Yes)
  event->accept();
 else
  event->ignore();
 return;
 }
//
void TMainWnd::aboutToQuit()
 {
 writeSettings();
 return;
 }
//
void TMainWnd::about()
 {
 QMessageBox::information(this,tr("About program"),tr("Program. Bla-bla about...\nCopyrigh (c) Denis P.Classen, from 2016"),QMessageBox::Ok);
 return;
 }
//
void TMainWnd::getList()
 {
 qDebug()<<"get list";
 TAppController appController;
 QList <WindowInfo> list=appController.getWindowList();
qDebug()<<list.size();
 windowsCombo->clear();
 for (int i=0;i<list.size();i++)
  {
  //qDebug()<<list.at(i).handle;
  windowsCombo->addItem(/*list.at(i).appTitle+" - "+*/list.at(i).windowTitle,(quint64)list.at(i).handle);
  }

 return;
 }
//
void TMainWnd::keyMapperMapped(int value)
 {
 //HWND handle=(HWND)windowsCombo->currentData();
 quint64 data=windowsCombo->currentData().toULongLong();
 HWND handle=(HWND)data;
 qDebug()<<QString::number(data,16).prepend("0x")<<value;

 TAppController appController;
 qDebug()<<appController.sendMessage(handle,(TAppController::Direction)value);
 return;
 }
//
/*void TMainWnd::down()
 {

 }
//
void TMainWnd::left()
 {

 }
//
void TMainWnd::right()
 {

 }*/
//
