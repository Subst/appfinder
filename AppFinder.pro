TEMPLATE = app
TARGET = AppFinder

QT += core \
      gui \
      widgets \
      xml
    
FORMS += ui/TMainWnd.ui
HEADERS += src/TMainWnd.h \
    src/TAppController.h

SOURCES += src/main.cpp \
           src/TAppController.cpp \
           src/TMainWnd.cpp

UI_DIR = ui_src
DESTDIR = bin
MOC_DIR = moc
OBJECTS_DIR = obj

RESOURCES += 
OTHER_FILES += 

LIBS += -lpsapi -lversion

QMAKE_CXXFLAGS += -Wno-unused-variable -Wno-unused-parameter -Wno-sign-compare# -Wno-attributes
